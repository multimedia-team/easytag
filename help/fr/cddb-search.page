<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="guide" id="cddb-search" xml:lang="fr">

  <info>
    <link type="guide" style="2column" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-10-07" status="review"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Remplir les étiquettes avec des données issues d'une base de données pour CD</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Recherche dans la CDDB</title>

  <p>Si votre musique est organisée en albums, <app>EasyTAG</app> peut explorer en ligne des bases de données utilisant le protocole <sys>CDDB</sys> et remplir automatiquement plusieurs champs d'étiquettes.</p>

  <steps>
    <item>
      <p><link xref="file-select">Sélectionnez tous les fichiers</link> dans l'album pour lesquels vous avez l'intention d'effectuer une recherche. L'ordre des fichiers doit correspondre à celui des pistes dans l'album.</p>
    </item>
    <item>
      <p>Sélectionnez <guiseq><gui style="menu">Divers</gui><gui style="menuitem">Recherche dans la CDDB…</gui></guiseq>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui style="button">Rechercher en utilisant les fichiers sélectionnés</gui> et attendez la fin de la recherche.</p>
    </item>
    <item>
      <!-- TODO Split Manual search into a tip? -->
      <p>Sélectionnez un album dans la liste de résultats, et l'information sur les pistes s'affiche à droite. Si l'album n'est pas celui que vous souhaitiez, essayez de sélectionner un autre résultat dans la liste.</p>
      <p>Si aucun résultat ne correspond aux pistes que vous avez sélectionnées, saisissez le nom de l'album dans le champ de recherche. Cliquez sur le bouton <gui style="button">Rechercher</gui>, attendez que la recherche soit terminée et examinez à nouveau les résultats pour voir si vous pouvez trouver un album correspondant aux pistes que vous avez sélectionnées.</p>
    </item>
    <item>
      <p>Choisissez les champs d'étiquettes à remplir en sélectionnant une combinaison de <gui style="checkbox">Nom de fichier</gui>, <gui style="checkbox">Titre</gui>, <gui style="checkbox">Artiste</gui>, <gui style="checkbox">Album</gui>, <gui style="checkbox">Année</gui>, <gui style="checkbox">N° de piste</gui>, <gui style="checkbox">Nombre de pistes</gui> et <gui style="checkbox">Genre</gui>.</p>
    </item>
    <!-- TODO Run the scanner for each file -->
    <!-- TODO Levenshtein matching -->
    <item>
      <p>Pour remplir les étiquettes des fichiers sélectionnés avec les données obtenues dans les résultats de recherche, cliquez sur <gui style="button">Appliquer</gui>.</p>
    </item>
  </steps>

  <p>Si vous désirez n'étiqueter que certains fichiers, vous pouvez modifier la sélection après la fin de la recherche.</p>

</page>
