<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="problems-ogg-split" xml:lang="de">

  <info>
    <link type="guide" xref="index#problems"/>
    <revision pkgversion="2.2.0" date="2014-04-11" status="candidate"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>David King</name>
      <email its:translate="no">amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Ein einzelnes Metainfofeld in mehrere Felder aufspalten, wenn Sie Ogg- und FLAC-Dateien speichern</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@gmail.com</mal:email>
      <mal:years>2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Metainfofelder beim Speichern aufspalten</title>

  <p>In Ogg- und FLAC-Dateien können mehrere <link xref="tag">Metainfo-Felder</link> jedes Types vorhanden sein. Zum Beispiel, wenn 2 Künstler ein Musikstück wiedergeben, können 2 Künstler-Felder in den Metainfos vorhanden sein. <app>EasyTAG</app> liest diese Felder ein und kombiniert diese automatisch zu einem einzigen Feld.</p>

  <p>Viele Anwendungen lesen Felder mit mehreren Werten nicht korrekt, deshalb wird per Vorgabe jeder Wert in einem einzelnen Feld gespeichert. Wenn die von Ihnen verwendete Anwendung mehrere Felder des selben Typs unterstützt, können Sie EasyTAG die Felder beim Speichern aufspalten lassen. Die Felder werden beim Vorkommen von » - « automatisch aufgespalten. Zum Beispiel wird das Feld »David Bowie - Queen« in die zwei Felder »David Bowie« und »Queen« aufgespalten.</p>

  <p>So teilen Sie Felder beim Speichern der Dateien auf:</p>

  <steps>
    <item>
      <p>Wählen Sie <guiseq style="menuitem"><gui style="menu">Bearbeiten</gui><gui style="menuitem">Einstellungen</gui></guiseq>.</p>
    </item>
    <item>
      <p>Wählen Sie den Reiter <gui style="tab">Metainformationen</gui></p>
    </item>
    <item>
      <p>Im Bereich <gui style="group">Trennen</gui> können Sie die beim Speichern aufzuspaltenden Felder auswählen.</p>
    </item>
  </steps>

</page>
