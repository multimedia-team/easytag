<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="format-specifier" xml:lang="ca">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-08-18" status="candidate"/>

    <credit type="author">
      <name>Ferran Roig</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Què és un Codi-de-format i quins es poden usar?</desc>

  </info>

  <title>Codis de format</title>

  <p>Els <em>Codis de format</em> es poden utilitzar, en l'<link xref="scanner">Explorador</link>, per dividir un nom de fitxer i introduir-lo a l'etiqueta i per anomenar un fitxer o una llista de reproducció basant-se en el contingut de l'etiqueta.</p>

  <table rules="rows" frame="top bottom">
    <thead>
      <tr>
        <td>
          <p>Codi de format</p>
        </td>
        <td>
          <p>Camp d'etiqueta corresponent</p>
        </td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <p its:translate="no">%a</p>
        </td>
        <td>
          <p>Artista: això és l'artista o artistes que han creat la pista, aquest camp pot tenir relació amb l'artista convidat o l'artista de l'àlbum</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%z</p>
        </td>
        <td>
          <p>Artista de l'àlbum</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%b</p>
        </td>
        <td>
          <p>Títol de l'àlbum</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%c</p>
        </td>
        <td>
          <p>Comentari: aquest és un camp de format lliure per comentaris</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%p</p>
        </td>
        <td>
          <p>Compositor: aquest és el creador de la pista</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%r</p>
        </td>
        <td>
          <p>Propietari del copyright: normalment és l'artista, però pot ser-ho la discogràfica</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%d</p>
        </td>
        <td>
          <p>Número de disc</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%e</p>
        </td>
        <td>
          <p>Codificat per: aquest camp fa referència a la persona que ha codificat el fitxer, però també s'utilitza pel nom de l'aplicació que s'ha utilitzat per codificar el fitxer</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%g</p>
        </td>
        <td>
          <p>Gènere</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%i</p>
        </td>
        <td>
          <p>Ignorat: aquest especificador de format només es pot emprar en <gui>Emplena etiqueta</gui> per especificar parts del nom de fitxer que s'ha d'ignorar</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%l</p>
        </td>
        <td>
          <p>Número de pistes: el número total de pistes de l'obra</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%o</p>
        </td>
        <td>
          <p>Artista original</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%n</p>
        </td>
        <td>
          <p>Número de pista</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%t</p>
        </td>
        <td>
          <p>Títol de la pista</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%u</p>
        </td>
        <td>
          <p>URL</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%x</p>
        </td>
        <td>
          <p>Número de discs</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%y</p>
        </td>
        <td>
          <p>Any de l'enregistrament, a vegades, data de llançament</p>
        </td>
      </tr>
    </tbody>
  </table>

</page>
