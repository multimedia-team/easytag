<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="guide" id="playlist-generate" xml:lang="cs">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-10-06" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak ze seznamu souborů vygenerovat seznam k přehrání, tzv. playlist</desc>
  </info>

  <title>Vytvoření seznamu k přehrání</title>

  <p>V aplikaci <app>EasyTAG</app> si můžete ze seznamu souborů vytvořit seznam k přehrání (tzv. playlist) M3U, který pak lze použít v hudebních přehrávačích.</p>

  <steps>
    <item>
      <p>V seznamu souborů <link xref="file-select">vyberte soubory</link>, které si přejete mít v seznamu k přehrání.</p>
    </item>
    <item>
      <p>Zvolte <guiseq><gui style="memu">Různé</gui> <gui style="menuitem">Vygenerovat seznam k přehrání…</gui></guiseq>.</p>
    </item>
    <item>
      <p>Vyberte <gui style="radiobutton">Použít masku</gui> a zadejte masku pomocí <link xref="format-specifier">formátovacích značek</link> nebo vyberte <gui style="radiobutton">Použít název složky</gui> k pojmenování seznamu k přehrání podle složky, která je vybrána v <gui style="group">prohlížeči</gui>.</p>
    </item>
    <item>
      <p>Zaškrtněte <gui style="checkbox">Zahrnout pouze vybrané soubory</gui>, aby se při generování seznamu k přehrání použily jen soubory, které jsou vybrané v seznamu souborů. Když zaškrtnutí zrušíte, budou součástí seznamu k přehrání všechny zobrazené soubory.</p>
    </item>
    <item>
      <p>Vyberte <gui style="radiobutton">Použít relativní cestu k souborům</gui>. Pouze pokud plánujete používat seznam k přehrání pořád na stejném počítači a nechystáte se zvukové soubory někam přesouvat, můžete vybrat <gui style="radiobutton">Použít plnou cestu k souborům</gui>.</p>
    </item>
    <item>
      <p>Zaškrtněte <gui style="checkbox">Vytvořit v nadřazené složce</gui>, pokud chcete uložit seznam k přehrání v rodičovské složce složky, která je vybraná v <gui style="group">Prohlížeči</gui>. Když volba zaškrtnutá není, ukládají se seznamy k přehrání přímo ve složce vybrané v <gui style="group">Prohlížeči</gui>.</p>
    </item>
    <item>
      <p>V případě, že vytváříte seznam k přehrání pro použití na počítači s Windows nebo na souborových systémech <sys>NTFS</sys> nebo <sys>FAT</sys>, zaškrtněte <gui style="checkbox">Použít oddělovač složek jako v systému DOS</gui>.</p>
    </item>
    <item>
      <p>Vyberte <gui style="radiobutton">Zapsat jen seznam souborů</gui>, aby se vytvořit seznam k přehrání, který bude obsahovat jen seznam souborů. Pokud vyberte <gui style="radiobutton">Zapsat informace za použití názvu souboru</gui>, zapíší se i rozšiřující informace, včetně délky zvukového souboru. Pomocí <gui style="radiobutton">Zapsat informace za použití</gui> a zadání masky z formátovacích značek můžete do seznamu k přehrání nechat zapsat vlastní rozšiřující informace.</p>
    </item>
    <item>
      <p>Výsledný seznam k přehrání vygenerujete zmáčknutím <gui style="button">Uložit</gui>.</p>
    </item>
  </steps>

</page>
