<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="problems-ogg-split" xml:lang="cs">

  <info>
    <link type="guide" xref="index#problems"/>
    <revision pkgversion="2.2.0" date="2014-04-11" status="candidate"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>David King</name>
      <email its:translate="no">amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Jak jedno pole štítku rozdělit do více polí při ukládání souborů Ogg a FLAC</desc>
  </info>

  <title>Rozdělení polí štítku při ukládání</title>

  <p>V souborech Ogg a FLAC je možné mít více než jedno <link xref="tag">pole štítku</link> stejného typu. Například, když píseň zpívají dva umělci, můžete mít ve štítku dvě pole s umělci, pro každého jedno. <app>EasyTAG</app> automaticky načítá dodatečná pole, spojuje je dohromady a zobrazuje je, jako by šlo o jedno pole.</p>

  <p>Spousta aplikaci nečte pole s více hodnotami správně, takže výchozí nastavení je ukládat jen jedno pole na záznam. Pokud aplikace, kterou používáte podporuje více polí stejného typu, můžete nastavit <app>EasyTAG</app>, aby pole při ukládání souboru rozděloval. Pole se automaticky rozdělí na každém výskytu „ - “. Například pole s „David Bowie - Queen“ by se uložilo do dvou polí: „David Bowie“ a „Queen“.</p>

  <p>Aby se pole při ukládání rozdělovala:</p>

  <steps>
    <item>
      <p>Vyberte <guiseq style="menuitem"><gui style="menu">Upravit</gui><gui style="menuitem">Předvolby</gui></guiseq>.</p>
    </item>
    <item>
      <p>Vyberte kartu <gui style="tab">Štítky</gui>.</p>
    </item>
    <item>
      <p>V části <gui style="group">Rozdělování</gui> zaškrtněte pole, která chcete při ukládání rozdělovat na více polí.</p>
    </item>
  </steps>

</page>
