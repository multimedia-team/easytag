#!/bin/sh

## this script extracts mime-types associated with easytag
## (by looking at the desktop file), and creates a
## <package>.mime file with these associations, but with
## a lower priority.

## this prevents easytag to take over associations for (e.g.)
## directories (which it can handle, but in a rather unconventional way)


INFILE=data/easytag.desktop.in
OUTFILE=debian/easytag.mime

filenotfound() {
  echo "cannot find file '$1'" 1>&2
  echo "run this script from the base-directory of easytag" 1>&2
  exit 1
}

test -e "${INFILE}" || filenotfound "${INFILE}"
test -e "${OUTFILE}" || filenotfound "${OUTFILE}"

cat "${INFILE}" \
	| egrep "^MimeType=" \
	| sed -e 's|^MimeType=||' \
	  -e 's|\([^;]*\);|\1; easytag %s; test=test -n "$DISPLAY"; priority=2\n|g' \
	| grep . > "${OUTFILE}"
