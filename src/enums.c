


#include "enums.h"

/* enumerations from "/home/david/checkout/gnome/easytag/src/setting.h" */
#include "/home/david/checkout/gnome/easytag/src/setting.h"
GType
et_cddb_search_category_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GFlagsValue _et_cddb_search_category_values[] = {
            { ET_CDDB_SEARCH_CATEGORY_BLUES, "ET_CDDB_SEARCH_CATEGORY_BLUES", "blues" },
            { ET_CDDB_SEARCH_CATEGORY_CLASSICAL, "ET_CDDB_SEARCH_CATEGORY_CLASSICAL", "classical" },
            { ET_CDDB_SEARCH_CATEGORY_COUNTRY, "ET_CDDB_SEARCH_CATEGORY_COUNTRY", "country" },
            { ET_CDDB_SEARCH_CATEGORY_FOLK, "ET_CDDB_SEARCH_CATEGORY_FOLK", "folk" },
            { ET_CDDB_SEARCH_CATEGORY_JAZZ, "ET_CDDB_SEARCH_CATEGORY_JAZZ", "jazz" },
            { ET_CDDB_SEARCH_CATEGORY_MISC, "ET_CDDB_SEARCH_CATEGORY_MISC", "misc" },
            { ET_CDDB_SEARCH_CATEGORY_NEWAGE, "ET_CDDB_SEARCH_CATEGORY_NEWAGE", "newage" },
            { ET_CDDB_SEARCH_CATEGORY_REGGAE, "ET_CDDB_SEARCH_CATEGORY_REGGAE", "reggae" },
            { ET_CDDB_SEARCH_CATEGORY_ROCK, "ET_CDDB_SEARCH_CATEGORY_ROCK", "rock" },
            { ET_CDDB_SEARCH_CATEGORY_SOUNDTRACK, "ET_CDDB_SEARCH_CATEGORY_SOUNDTRACK", "soundtrack" },
            { 0, NULL, NULL }
        };

        type = g_flags_register_static ("EtCddbSearchCategory", _et_cddb_search_category_values);
    }

  return type;
}

GType
et_cddb_search_field_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GFlagsValue _et_cddb_search_field_values[] = {
            { ET_CDDB_SEARCH_FIELD_ARTIST, "ET_CDDB_SEARCH_FIELD_ARTIST", "artist" },
            { ET_CDDB_SEARCH_FIELD_TITLE, "ET_CDDB_SEARCH_FIELD_TITLE", "title" },
            { ET_CDDB_SEARCH_FIELD_TRACK, "ET_CDDB_SEARCH_FIELD_TRACK", "track" },
            { ET_CDDB_SEARCH_FIELD_OTHER, "ET_CDDB_SEARCH_FIELD_OTHER", "other" },
            { 0, NULL, NULL }
        };

        type = g_flags_register_static ("EtCddbSearchField", _et_cddb_search_field_values);
    }

  return type;
}

GType
et_cddb_set_field_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GFlagsValue _et_cddb_set_field_values[] = {
            { ET_CDDB_SET_FIELD_TITLE, "ET_CDDB_SET_FIELD_TITLE", "title" },
            { ET_CDDB_SET_FIELD_ARTIST, "ET_CDDB_SET_FIELD_ARTIST", "artist" },
            { ET_CDDB_SET_FIELD_ALBUM, "ET_CDDB_SET_FIELD_ALBUM", "album" },
            { ET_CDDB_SET_FIELD_YEAR, "ET_CDDB_SET_FIELD_YEAR", "year" },
            { ET_CDDB_SET_FIELD_TRACK, "ET_CDDB_SET_FIELD_TRACK", "track" },
            { ET_CDDB_SET_FIELD_TRACK_TOTAL, "ET_CDDB_SET_FIELD_TRACK_TOTAL", "track-total" },
            { ET_CDDB_SET_FIELD_GENRE, "ET_CDDB_SET_FIELD_GENRE", "genre" },
            { ET_CDDB_SET_FIELD_FILENAME, "ET_CDDB_SET_FIELD_FILENAME", "filename" },
            { 0, NULL, NULL }
        };

        type = g_flags_register_static ("EtCddbSetField", _et_cddb_set_field_values);
    }

  return type;
}

GType
et_charset_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_charset_values[] = {
            { ET_CHARSET_IBM864, "ET_CHARSET_IBM864", "ibm864" },
            { ET_CHARSET_ISO_8859_6, "ET_CHARSET_ISO_8859_6", "iso-8859-6" },
            { ET_CHARSET_WINDOWS_1256, "ET_CHARSET_WINDOWS_1256", "windows-1256" },
            { ET_CHARSET_ISO_8859_13, "ET_CHARSET_ISO_8859_13", "iso-8859-13" },
            { ET_CHARSET_ISO_8859_4, "ET_CHARSET_ISO_8859_4", "iso-8859-4" },
            { ET_CHARSET_WINDOWS_1257, "ET_CHARSET_WINDOWS_1257", "windows-1257" },
            { ET_CHARSET_ISO_8859_14, "ET_CHARSET_ISO_8859_14", "iso-8859-14" },
            { ET_CHARSET_IBM852, "ET_CHARSET_IBM852", "ibm852" },
            { ET_CHARSET_ISO_8859_2, "ET_CHARSET_ISO_8859_2", "iso-8859-2" },
            { ET_CHARSET_WINDOWS_1250, "ET_CHARSET_WINDOWS_1250", "windows-1250" },
            { ET_CHARSET_GB18030, "ET_CHARSET_GB18030", "gb18030" },
            { ET_CHARSET_GB2312, "ET_CHARSET_GB2312", "gb2312" },
            { ET_CHARSET_BIG5, "ET_CHARSET_BIG5", "big5" },
            { ET_CHARSET_BIG5_HKSCS, "ET_CHARSET_BIG5_HKSCS", "big5-hkscs" },
            { ET_CHARSET_IBM855, "ET_CHARSET_IBM855", "ibm855" },
            { ET_CHARSET_ISO_8859_5, "ET_CHARSET_ISO_8859_5", "iso-8859-5" },
            { ET_CHARSET_ISO_IR_111, "ET_CHARSET_ISO_IR_111", "iso-ir-111" },
            { ET_CHARSET_ISO_KOI8_R, "ET_CHARSET_ISO_KOI8_R", "iso-koi8-r" },
            { ET_CHARSET_WINDOWS_1251, "ET_CHARSET_WINDOWS_1251", "windows-1251" },
            { ET_CHARSET_IBM866, "ET_CHARSET_IBM866", "ibm866" },
            { ET_CHARSET_KOI8_U, "ET_CHARSET_KOI8_U", "koi8-u" },
            { ET_CHARSET_US_ASCII, "ET_CHARSET_US_ASCII", "us-ascii" },
            { ET_CHARSET_ISO_8859_7, "ET_CHARSET_ISO_8859_7", "iso-8859-7" },
            { ET_CHARSET_WINDOWS_1253, "ET_CHARSET_WINDOWS_1253", "windows-1253" },
            { ET_CHARSET_IBM862, "ET_CHARSET_IBM862", "ibm862" },
            { ET_CHARSET_WINDOWS_1255, "ET_CHARSET_WINDOWS_1255", "windows-1255" },
            { ET_CHARSET_EUC_JP, "ET_CHARSET_EUC_JP", "euc-jp" },
            { ET_CHARSET_ISO_2022_JP, "ET_CHARSET_ISO_2022_JP", "iso-2022-jp" },
            { ET_CHARSET_SHIFT_JIS, "ET_CHARSET_SHIFT_JIS", "shift-jis" },
            { ET_CHARSET_EUC_KR, "ET_CHARSET_EUC_KR", "euc-kr" },
            { ET_CHARSET_ISO_8859_10, "ET_CHARSET_ISO_8859_10", "iso-8859-10" },
            { ET_CHARSET_ISO_8859_3, "ET_CHARSET_ISO_8859_3", "iso-8859-3" },
            { ET_CHARSET_TIS_620, "ET_CHARSET_TIS_620", "tis-620" },
            { ET_CHARSET_IBM857, "ET_CHARSET_IBM857", "ibm857" },
            { ET_CHARSET_ISO_8859_9, "ET_CHARSET_ISO_8859_9", "iso-8859-9" },
            { ET_CHARSET_WINDOWS_1254, "ET_CHARSET_WINDOWS_1254", "windows-1254" },
            { ET_CHARSET_UTF_8, "ET_CHARSET_UTF_8", "utf-8" },
            { ET_CHARSET_VISCII, "ET_CHARSET_VISCII", "viscii" },
            { ET_CHARSET_WINDOWS_1258, "ET_CHARSET_WINDOWS_1258", "windows-1258" },
            { ET_CHARSET_ISO_8859_8, "ET_CHARSET_ISO_8859_8", "iso-8859-8" },
            { ET_CHARSET_IBM850, "ET_CHARSET_IBM850", "ibm850" },
            { ET_CHARSET_ISO_8859_1, "ET_CHARSET_ISO_8859_1", "iso-8859-1" },
            { ET_CHARSET_ISO_8859_15, "ET_CHARSET_ISO_8859_15", "iso-8859-15" },
            { ET_CHARSET_WINDOWS_1252, "ET_CHARSET_WINDOWS_1252", "windows-1252" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtCharset", _et_charset_values);
    }

  return type;
}

GType
et_convert_spaces_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_convert_spaces_values[] = {
            { ET_CONVERT_SPACES_SPACES, "ET_CONVERT_SPACES_SPACES", "spaces" },
            { ET_CONVERT_SPACES_UNDERSCORES, "ET_CONVERT_SPACES_UNDERSCORES", "underscores" },
            { ET_CONVERT_SPACES_REMOVE, "ET_CONVERT_SPACES_REMOVE", "remove" },
            { ET_CONVERT_SPACES_NO_CHANGE, "ET_CONVERT_SPACES_NO_CHANGE", "no-change" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtConvertSpaces", _et_convert_spaces_values);
    }

  return type;
}

GType
et_filename_extension_mode_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_filename_extension_mode_values[] = {
            { ET_FILENAME_EXTENSION_LOWER_CASE, "ET_FILENAME_EXTENSION_LOWER_CASE", "lower-case" },
            { ET_FILENAME_EXTENSION_UPPER_CASE, "ET_FILENAME_EXTENSION_UPPER_CASE", "upper-case" },
            { ET_FILENAME_EXTENSION_NO_CHANGE, "ET_FILENAME_EXTENSION_NO_CHANGE", "no-change" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtFilenameExtensionMode", _et_filename_extension_mode_values);
    }

  return type;
}

GType
et_process_field_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GFlagsValue _et_process_field_values[] = {
            { ET_PROCESS_FIELD_FILENAME, "ET_PROCESS_FIELD_FILENAME", "filename" },
            { ET_PROCESS_FIELD_TITLE, "ET_PROCESS_FIELD_TITLE", "title" },
            { ET_PROCESS_FIELD_ARTIST, "ET_PROCESS_FIELD_ARTIST", "artist" },
            { ET_PROCESS_FIELD_ALBUM_ARTIST, "ET_PROCESS_FIELD_ALBUM_ARTIST", "album-artist" },
            { ET_PROCESS_FIELD_ALBUM, "ET_PROCESS_FIELD_ALBUM", "album" },
            { ET_PROCESS_FIELD_GENRE, "ET_PROCESS_FIELD_GENRE", "genre" },
            { ET_PROCESS_FIELD_COMMENT, "ET_PROCESS_FIELD_COMMENT", "comment" },
            { ET_PROCESS_FIELD_COMPOSER, "ET_PROCESS_FIELD_COMPOSER", "composer" },
            { ET_PROCESS_FIELD_ORIGINAL_ARTIST, "ET_PROCESS_FIELD_ORIGINAL_ARTIST", "original-artist" },
            { ET_PROCESS_FIELD_COPYRIGHT, "ET_PROCESS_FIELD_COPYRIGHT", "copyright" },
            { ET_PROCESS_FIELD_URL, "ET_PROCESS_FIELD_URL", "url" },
            { ET_PROCESS_FIELD_ENCODED_BY, "ET_PROCESS_FIELD_ENCODED_BY", "encoded-by" },
            { 0, NULL, NULL }
        };

        type = g_flags_register_static ("EtProcessField", _et_process_field_values);
    }

  return type;
}

GType
et_process_fields_convert_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_process_fields_convert_values[] = {
            { ET_PROCESS_FIELDS_CONVERT_SPACES, "ET_PROCESS_FIELDS_CONVERT_SPACES", "spaces" },
            { ET_PROCESS_FIELDS_CONVERT_UNDERSCORES, "ET_PROCESS_FIELDS_CONVERT_UNDERSCORES", "underscores" },
            { ET_PROCESS_FIELDS_CONVERT_CHARACTERS, "ET_PROCESS_FIELDS_CONVERT_CHARACTERS", "characters" },
            { ET_PROCESS_FIELDS_CONVERT_NO_CHANGE, "ET_PROCESS_FIELDS_CONVERT_NO_CHANGE", "no-change" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtProcessFieldsConvert", _et_process_fields_convert_values);
    }

  return type;
}

GType
et_playlist_content_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_playlist_content_values[] = {
            { ET_PLAYLIST_CONTENT_FILENAMES, "ET_PLAYLIST_CONTENT_FILENAMES", "filenames" },
            { ET_PLAYLIST_CONTENT_EXTENDED, "ET_PLAYLIST_CONTENT_EXTENDED", "extended" },
            { ET_PLAYLIST_CONTENT_EXTENDED_MASK, "ET_PLAYLIST_CONTENT_EXTENDED_MASK", "extended-mask" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtPlaylistContent", _et_playlist_content_values);
    }

  return type;
}

GType
et_rename_encoding_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_rename_encoding_values[] = {
            { ET_RENAME_ENCODING_TRY_ALTERNATIVE, "ET_RENAME_ENCODING_TRY_ALTERNATIVE", "try-alternative" },
            { ET_RENAME_ENCODING_TRANSLITERATE, "ET_RENAME_ENCODING_TRANSLITERATE", "transliterate" },
            { ET_RENAME_ENCODING_IGNORE, "ET_RENAME_ENCODING_IGNORE", "ignore" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtRenameEncoding", _et_rename_encoding_values);
    }

  return type;
}

GType
et_scan_mode_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_scan_mode_values[] = {
            { ET_SCAN_MODE_FILL_TAG, "ET_SCAN_MODE_FILL_TAG", "fill-tag" },
            { ET_SCAN_MODE_RENAME_FILE, "ET_SCAN_MODE_RENAME_FILE", "rename-file" },
            { ET_SCAN_MODE_PROCESS_FIELDS, "ET_SCAN_MODE_PROCESS_FIELDS", "process-fields" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtScanMode", _et_scan_mode_values);
    }

  return type;
}

GType
et_sort_mode_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_sort_mode_values[] = {
            { ET_SORT_MODE_ASCENDING_FILENAME, "ET_SORT_MODE_ASCENDING_FILENAME", "ascending-filename" },
            { ET_SORT_MODE_DESCENDING_FILENAME, "ET_SORT_MODE_DESCENDING_FILENAME", "descending-filename" },
            { ET_SORT_MODE_ASCENDING_TITLE, "ET_SORT_MODE_ASCENDING_TITLE", "ascending-title" },
            { ET_SORT_MODE_DESCENDING_TITLE, "ET_SORT_MODE_DESCENDING_TITLE", "descending-title" },
            { ET_SORT_MODE_ASCENDING_ARTIST, "ET_SORT_MODE_ASCENDING_ARTIST", "ascending-artist" },
            { ET_SORT_MODE_DESCENDING_ARTIST, "ET_SORT_MODE_DESCENDING_ARTIST", "descending-artist" },
            { ET_SORT_MODE_ASCENDING_ALBUM_ARTIST, "ET_SORT_MODE_ASCENDING_ALBUM_ARTIST", "ascending-album-artist" },
            { ET_SORT_MODE_DESCENDING_ALBUM_ARTIST, "ET_SORT_MODE_DESCENDING_ALBUM_ARTIST", "descending-album-artist" },
            { ET_SORT_MODE_ASCENDING_ALBUM, "ET_SORT_MODE_ASCENDING_ALBUM", "ascending-album" },
            { ET_SORT_MODE_DESCENDING_ALBUM, "ET_SORT_MODE_DESCENDING_ALBUM", "descending-album" },
            { ET_SORT_MODE_ASCENDING_YEAR, "ET_SORT_MODE_ASCENDING_YEAR", "ascending-year" },
            { ET_SORT_MODE_DESCENDING_YEAR, "ET_SORT_MODE_DESCENDING_YEAR", "descending-year" },
            { ET_SORT_MODE_ASCENDING_DISC_NUMBER, "ET_SORT_MODE_ASCENDING_DISC_NUMBER", "ascending-disc-number" },
            { ET_SORT_MODE_DESCENDING_DISC_NUMBER, "ET_SORT_MODE_DESCENDING_DISC_NUMBER", "descending-disc-number" },
            { ET_SORT_MODE_ASCENDING_TRACK_NUMBER, "ET_SORT_MODE_ASCENDING_TRACK_NUMBER", "ascending-track-number" },
            { ET_SORT_MODE_DESCENDING_TRACK_NUMBER, "ET_SORT_MODE_DESCENDING_TRACK_NUMBER", "descending-track-number" },
            { ET_SORT_MODE_ASCENDING_GENRE, "ET_SORT_MODE_ASCENDING_GENRE", "ascending-genre" },
            { ET_SORT_MODE_DESCENDING_GENRE, "ET_SORT_MODE_DESCENDING_GENRE", "descending-genre" },
            { ET_SORT_MODE_ASCENDING_COMMENT, "ET_SORT_MODE_ASCENDING_COMMENT", "ascending-comment" },
            { ET_SORT_MODE_DESCENDING_COMMENT, "ET_SORT_MODE_DESCENDING_COMMENT", "descending-comment" },
            { ET_SORT_MODE_ASCENDING_COMPOSER, "ET_SORT_MODE_ASCENDING_COMPOSER", "ascending-composer" },
            { ET_SORT_MODE_DESCENDING_COMPOSER, "ET_SORT_MODE_DESCENDING_COMPOSER", "descending-composer" },
            { ET_SORT_MODE_ASCENDING_ORIG_ARTIST, "ET_SORT_MODE_ASCENDING_ORIG_ARTIST", "ascending-orig-artist" },
            { ET_SORT_MODE_DESCENDING_ORIG_ARTIST, "ET_SORT_MODE_DESCENDING_ORIG_ARTIST", "descending-orig-artist" },
            { ET_SORT_MODE_ASCENDING_COPYRIGHT, "ET_SORT_MODE_ASCENDING_COPYRIGHT", "ascending-copyright" },
            { ET_SORT_MODE_DESCENDING_COPYRIGHT, "ET_SORT_MODE_DESCENDING_COPYRIGHT", "descending-copyright" },
            { ET_SORT_MODE_ASCENDING_URL, "ET_SORT_MODE_ASCENDING_URL", "ascending-url" },
            { ET_SORT_MODE_DESCENDING_URL, "ET_SORT_MODE_DESCENDING_URL", "descending-url" },
            { ET_SORT_MODE_ASCENDING_ENCODED_BY, "ET_SORT_MODE_ASCENDING_ENCODED_BY", "ascending-encoded-by" },
            { ET_SORT_MODE_DESCENDING_ENCODED_BY, "ET_SORT_MODE_DESCENDING_ENCODED_BY", "descending-encoded-by" },
            { ET_SORT_MODE_ASCENDING_CREATION_DATE, "ET_SORT_MODE_ASCENDING_CREATION_DATE", "ascending-creation-date" },
            { ET_SORT_MODE_DESCENDING_CREATION_DATE, "ET_SORT_MODE_DESCENDING_CREATION_DATE", "descending-creation-date" },
            { ET_SORT_MODE_ASCENDING_FILE_TYPE, "ET_SORT_MODE_ASCENDING_FILE_TYPE", "ascending-file-type" },
            { ET_SORT_MODE_DESCENDING_FILE_TYPE, "ET_SORT_MODE_DESCENDING_FILE_TYPE", "descending-file-type" },
            { ET_SORT_MODE_ASCENDING_FILE_SIZE, "ET_SORT_MODE_ASCENDING_FILE_SIZE", "ascending-file-size" },
            { ET_SORT_MODE_DESCENDING_FILE_SIZE, "ET_SORT_MODE_DESCENDING_FILE_SIZE", "descending-file-size" },
            { ET_SORT_MODE_ASCENDING_FILE_DURATION, "ET_SORT_MODE_ASCENDING_FILE_DURATION", "ascending-file-duration" },
            { ET_SORT_MODE_DESCENDING_FILE_DURATION, "ET_SORT_MODE_DESCENDING_FILE_DURATION", "descending-file-duration" },
            { ET_SORT_MODE_ASCENDING_FILE_BITRATE, "ET_SORT_MODE_ASCENDING_FILE_BITRATE", "ascending-file-bitrate" },
            { ET_SORT_MODE_DESCENDING_FILE_BITRATE, "ET_SORT_MODE_DESCENDING_FILE_BITRATE", "descending-file-bitrate" },
            { ET_SORT_MODE_ASCENDING_FILE_SAMPLERATE, "ET_SORT_MODE_ASCENDING_FILE_SAMPLERATE", "ascending-file-samplerate" },
            { ET_SORT_MODE_DESCENDING_FILE_SAMPLERATE, "ET_SORT_MODE_DESCENDING_FILE_SAMPLERATE", "descending-file-samplerate" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtSortMode", _et_sort_mode_values);
    }

  return type;
}

GType
et_tag_encoding_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        static const GEnumValue _et_tag_encoding_values[] = {
            { ET_TAG_ENCODING_NONE, "ET_TAG_ENCODING_NONE", "none" },
            { ET_TAG_ENCODING_TRANSLITERATE, "ET_TAG_ENCODING_TRANSLITERATE", "transliterate" },
            { ET_TAG_ENCODING_IGNORE, "ET_TAG_ENCODING_IGNORE", "ignore" },
            { 0, NULL, NULL }
        };

        type = g_enum_register_static ("EtTagEncoding", _et_tag_encoding_values);
    }

  return type;
}




